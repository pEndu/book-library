package com.example.booklibrary.controller;

import com.example.booklibrary.domain.Book;
import com.example.booklibrary.service.BookService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;

@Controller
public class AddController {
    private BookService bookService;

    private static final Logger logger = LoggerFactory.getLogger(AddController.class);

    @Autowired
    public AddController(BookService bookService) {
        this.bookService = bookService;
    }

    @RequestMapping("/add")
    public String listBooks(Model model) {
        model.addAttribute("book", new Book());
        logger.info("View changed to add new book");
        return "add";
    }

    @PostMapping(value = {"/"})
    public ModelAndView addBook(@Valid Book book, BindingResult result) {
        ModelAndView modelAndView = new ModelAndView();
        if (result.hasErrors()) {
            logger.warn("Adding book unsuccesful");
            modelAndView.setViewName("add");
            modelAndView.addObject("book", book);
            return modelAndView;
        }
        logger.info("Adding new book:"+book.toString());
        this.bookService.addBook(book);

        modelAndView.addObject("books", bookService.getAllBooks());
        modelAndView.setViewName("/index");
        return modelAndView;

    }
}
