package com.example.booklibrary.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.Pattern;

@Entity
public class Book {

    @Id
    @GeneratedValue
    private Long id;

    @Pattern(regexp = ".*\\b[A].*", message="Author forename or surname has to start with A.")
    private String author;

    private String title;

    @Pattern(regexp = "(0|[0-9]*)$", message="In ISBN there can be only digits.")
    private String isbn;  //since ISBN first digit can be 0, it cannot be numeric type

    public Book() {
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    @Override
    public String toString() {
        return "Book:{" +
                " author='" + author + '\'' +
                ", title='" + title + '\'' +
                ", isbn='" + isbn + '\'' +
                '}';
    }
}
